﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace myinterfaces
{
    class Car:IMovable
    {
        public void Move()
        {
            Console.WriteLine("Move with car");
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace myinterfaces
{
    interface IMovable
    {
        void Move();
    }
}

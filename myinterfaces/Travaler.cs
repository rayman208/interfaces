﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace myinterfaces
{
    class Travaler
    {
        private IMovable vehicle;

        public Travaler(IMovable vehicle)
        {
            this.vehicle = vehicle;
        }

        public void Move()
        {
            vehicle.Move();
        }

        public void SetNewVehicle(IMovable vehicle)
        {
            this.vehicle = vehicle;
        }

    }
}

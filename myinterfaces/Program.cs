﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace myinterfaces
{
    class Program
    {
        static void Main(string[] args)
        {
            /*IMovable[] movables = new IMovable[2];

            movables[0] = new Car();
            movables[1] = new Legs();

            for (int i = 0; i < movables.Length; i++)
            {
                movables[i].Move();
            }*/

            Travaler travaler = new Travaler(new Car());
            travaler.Move();

            travaler.SetNewVehicle(new Plane());
            travaler.Move();

            Console.ReadKey();
        }
    }
}
